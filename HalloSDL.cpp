#include "HalloSDL.h"

HalloSDL::HalloSDL() {

	//The window we'll be rendering to 
	window = NULL;
	//The surface contained by the window 
	screenSurface = NULL;

}

void HalloSDL::display(){

	//Initialize SDL 
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else {

		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		//Create window 
		window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL);
		if (window == NULL) {
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		}
		else {
			//Get window surface 
			screenSurface = SDL_GetWindowSurface(window);
			//Fill the surface white 
			SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0x90, 0x7F, 0xA7));
			//Update the surface 
			SDL_UpdateWindowSurface(window);

		}
		
	}

}//end fun display()


HalloSDL::~HalloSDL() {

}

void HalloSDL::destroy_after_delay() {
	//Destroy window 
	SDL_DestroyWindow(window);
	//Quit SDL subsystems 
	SDL_Quit();
}


