#include "Image_on_screen.h"


Image_on_screen::Image_on_screen() {
	//The window we'll be rendering to
	gWindow = NULL;

	//The surface contained by the window
	gScreenSurface = NULL;

	//The image we will load and show on the screen
	gHelloWorld = NULL;
}


Image_on_screen::~Image_on_screen(){}

bool Image_on_screen::init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Create window
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Get window surface
			gScreenSurface = SDL_GetWindowSurface(gWindow);
		}
	}

	return success;
}

bool Image_on_screen::loadMedia() {
	//Loading success flag
	bool success = true;

	//Load splash image
	gHelloWorld = SDL_LoadBMP("img/hello_world.bmp");
	if (gHelloWorld == NULL)
	{
		printf("Unable to load image %s! SDL Error: %s\n", "img/hello_world.bmp", SDL_GetError());
		success = false;
	}

	return success;
}

void Image_on_screen::display(){
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Load media
		if (!loadMedia())
		{
			printf("Failed to load media!\n");
		}
		else
		{
			//Apply the image
			SDL_BlitSurface(getHelloWorld(), NULL, getScreenSurface(), NULL);

			//Update the surface
			SDL_UpdateWindowSurface(getWindow());

		}
	}
}

void Image_on_screen::destroy_after_delay() {
	//Deallocate surface
	SDL_FreeSurface(gHelloWorld);
	gHelloWorld = NULL;

	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}


//The window we'll be rendering to
SDL_Window* Image_on_screen::getWindow(){
	return gWindow;
}

//The surface contained by the window
SDL_Surface* Image_on_screen::getScreenSurface(){
	return gScreenSurface;
}

//The image we will load and show on the screen
SDL_Surface* Image_on_screen::getHelloWorld(){
	return gHelloWorld;
}

