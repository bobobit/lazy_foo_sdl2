#pragma once

//Using SDL and standard IO 
#include <SDL2/SDL.h> 
//#undef main 
#include <stdio.h> 

class Events
{
public:
	Events();
	~Events();

	//Starts up SDL and creates window
	bool init();

	//Loads media
	bool loadMedia();

	//Display image
	void display();

	//Frees media and shuts down SDL
	void destroy_after_delay();

	//The window we'll be rendering to
	SDL_Window* getWindow();

	//The surface contained by the window
	SDL_Surface* getScreenSurface();

	//The image we will load and show on the screen
	SDL_Surface* getHelloWorld();



	//Screen dimension constants 
	const int SCREEN_WIDTH = 640;
	const int SCREEN_HEIGHT = 480;
private:
	//The window we'll be rendering to
	SDL_Window* gWindow;

	//The surface contained by the window
	SDL_Surface* gScreenSurface;

	//The image we will load and show on the screen
	SDL_Surface* gHelloWorld;
};

