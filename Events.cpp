#include "Events.h"

Events::Events() {
	//The window we'll be rendering to
	gWindow = NULL;

	//The surface contained by the window
	gScreenSurface = NULL;

	//The image we will load and show on the screen
	gHelloWorld = NULL;
}


Events::~Events(){
	destroy_after_delay();
}

bool Events::init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Create window
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Get window surface
			gScreenSurface = SDL_GetWindowSurface(gWindow);
		}
	}

	return success;
}

bool Events::loadMedia() {
	//Loading success flag
	bool success = true;

	//Load splash image
	gHelloWorld = SDL_LoadBMP("img/x.bmp");
	if (gHelloWorld == NULL)
	{
		printf("Unable to load image %s! SDL Error: %s\n", "img/x.bmp", SDL_GetError());
		success = false;
	}

	return success;
}

void Events::display(){
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Load media
		if (!loadMedia())
		{
			printf("Failed to load media!\n");
		}
		else
		{


			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			//While application is running
			while (!quit)
			{
				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
				}

				//Apply the image
				SDL_BlitSurface(getHelloWorld(), NULL, getScreenSurface(), NULL);

				//Update the surface
				SDL_UpdateWindowSurface(getWindow());
			}



		}
	}
}

void Events::destroy_after_delay() {
	//Deallocate surface
	SDL_FreeSurface(gHelloWorld);
	gHelloWorld = NULL;

	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Quit SDL subsystems
	SDL_Quit();

	printf("Destroed Surface, Window and Quit SDL\n");
}


//The window we'll be rendering to
SDL_Window* Events::getWindow(){
	return gWindow;
}

//The surface contained by the window
SDL_Surface* Events::getScreenSurface(){
	return gScreenSurface;
}

//The image we will load and show on the screen
SDL_Surface* Events::getHelloWorld(){
	return gHelloWorld;
}


