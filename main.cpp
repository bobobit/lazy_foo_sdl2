#include "HalloSDL.h"
#include "Image_on_screen.h"
#include "Events.h"
#include "MyKeyPress.h"
#include "StretchingImage.h"
#include "ImageFormats.h"
#include "TextureRendering.h"
#include "GeometryRendering.h"
#include "Viewport.h"
#include "ColorKeying.h"



#include <iostream>
#include <string>
using namespace std;

//Declare custom functions before main()
void hallo_sdl();
void image_on_screen();
void events();
void key_press();
void stretching_img();
void image_formats();
void texture_rendering();
void geometry_rendering();
void viewport();
void color_keying();





int main(int argc, char* args[]) { 
	
	const int TUTS_COUNT = 10;
	string *tutorials = new string[TUTS_COUNT];

	tutorials[0] = "Hallo SDL";
	tutorials[1] = "Image on screen";
	tutorials[2] = "Events";
	tutorials[3] = "KeyPress";
	tutorials[4] = "Stretching image";
	tutorials[5] = "Image Formats";
	tutorials[6] = "Texture Rendering";
	tutorials[7] = "Geometry Rendering";
	tutorials[8] = "Viewport";
	tutorials[9] = "Color Keying";
	
	
	
	
		
	cout << "Tutorials: " << endl;
	for (int i = 0; i < TUTS_COUNT; i++) {
		cout << i + 1 << ". " << tutorials[i] << endl;
	}
	int choice;
	cout << "Choose tutorial number?: ";
	cin >> choice;
	cout << endl;

	switch (choice-1)	{

		case 0: hallo_sdl(); break;				//1
		case 1: image_on_screen(); break;		//2
		case 2: events(); break;				//3
		case 3: key_press(); break;				//4
		case 4: stretching_img(); break;		//5
		case 5: image_formats(); break;			//6
		case 6: texture_rendering(); break;		//7
		case 7: geometry_rendering(); break;	//8
		case 8: viewport(); break;				//9
		case 9: color_keying(); break;			//10
			
			


		default:
			cout << "Tutorial you choose does not exists!" << endl;
			break;
	}


	//Delete tutorials array
	delete[] tutorials;


	return 0;
}//end main

//###
//Custom Functions
//###

void hallo_sdl(){
	//Hallo SDL
	HalloSDL hallosdl;
	//Display window Hallo SDL
	hallosdl.display();
	//Wait two seconds 
	SDL_Delay(2000);
	//Destroy window and Quit SDL subsystems
	hallosdl.destroy_after_delay();
}

void image_on_screen() {
	//Image on screen
	Image_on_screen image_on_screen;
	//Display window image_on_screen
	image_on_screen.display();
	//Wait two seconds
	SDL_Delay(4000);
	//Free resources and close SDL
	image_on_screen.destroy_after_delay();
}

void events(){
	//Events
	Events events;
	//Display window image_on_screen
	events.display();
}

void key_press(){
	//KeyPress
	MyKeyPress key_press;
	//Display window
	key_press.display();
}

void stretching_img(){
	//Stretching Image
	StretchingImage stretchingimage;
	//Display window
	stretchingimage.display();
}

void image_formats(){
	//Image Formats
	ImageFormats imageformats;
	//Display window
	imageformats.display();
}

void texture_rendering(){
	//Texture Rendering
	TextureRendering texturerendering;
	//Display window
	texturerendering.display();
}

void geometry_rendering(){
	//Geometry Rendering
	GeometryRendering geometryrendering;
	//Display window
	geometryrendering.display();
}


void viewport(){
	//Viewport
	Viewport viewport;
	//Display window
	viewport.display();
}

void color_keying(){
	//Color Keying
	ColorKeying colorkeying;
	//Display window
	colorkeying.display();
}

