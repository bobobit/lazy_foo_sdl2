#pragma once
//Using SDL and standard IO 
#include <SDL2/SDL.h> 
//#undef main 
#include <stdio.h> 

class HalloSDL
{
public:
	HalloSDL();
	~HalloSDL();
	void display();
	void destroy_after_delay();
	//Screen dimension constants 
	const int SCREEN_WIDTH = 640;
	const int SCREEN_HEIGHT = 480;
private:
	//The window we'll be rendering to 
	SDL_Window* window;
	//The surface contained by the window 
	SDL_Surface* screenSurface;
};

