#pragma once

#include <string>
#include <SDL2/SDL.h>

class LTexture {
public:
public:
	//Initializes variables
	LTexture();

	//Deallocates memory
	~LTexture();

	//Loads image at specified path
	bool loadFromFile(std::string path);

	//Deallocates texture
	void free();

	//Renders texture at given point
	void render(int x, int y);

	//Gets image dimensions
	int getWidth();
	int getHeight();

	//Get reference of window gRenderer
	SDL_Renderer* getWindowRenderer();

	//Set window gRenderer
	void setWindowRenderer(SDL_Renderer*);

private:
	//The actual hardware texture
	SDL_Texture* mTexture;

	//Image dimensions
	int mWidth;
	int mHeight;

	//The window renderer
	SDL_Renderer* gRenderer;
};

