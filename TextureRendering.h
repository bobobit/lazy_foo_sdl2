#pragma once

//Using SDL, SDL_image, standard IO, and strings
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>

class TextureRendering {
	public:
		TextureRendering();
		~TextureRendering();

		//Display
		void display();

		//Screen dimension constants
		const int SCREEN_WIDTH = 640;
		const int SCREEN_HEIGHT = 480;

	private:

		//Starts up SDL and creates window
		bool init();

		//Loads media
		bool loadMedia();

		//Frees media and shuts down SDL
		void close();

		//Loads individual image as texture
		SDL_Texture* loadTexture(std::string path);

		//The window we'll be rendering to
		SDL_Window* gWindow;

		//The window renderer
		SDL_Renderer* gRenderer;

		//Current displayed texture
		SDL_Texture* gTexture;
};

